package com.home.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;

import com.home.model.IpPublic;
import com.home.model.Mail;
import com.home.service.ServiceDao;
import com.home.service.ServiceFile;
import com.home.service.ServiceFtp;
import com.home.service.ServiceIpPublic;
import com.home.service.ServiceMail;

@Component

@ContextConfiguration(locations = "classpath:configuration.xml")
public class EnviadorMail {

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath*:com/home/main/configuration.xml");
		ServiceDao serviceDao = (ServiceDao) context.getBean("serviceDao");
		ServiceIpPublic serviceIpPublic = (ServiceIpPublic) context.getBean("serviceIpPublic");
		ServiceMail serviceMail = (ServiceMail) context.getBean("serviceMail");
		ServiceFile serviceFile = (ServiceFile) context.getBean("serviceFile");
		ServiceFtp serviceFtp = (ServiceFtp) context.getBean("serviceFtp");
		IpPublic ipPublic = serviceIpPublic.getPublicIp();
		Mail mail = Mail.getMail();
		mail.setCuerpo(ipPublic.getIp());
//		serviceMail.send(mail);
//		try {
//			serviceDao.saveIp(ipPublic);
//			serviceFile.saveFile(ipPublic);
//			serviceFtp.uploadIp();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
	}
}