package com.home.service;


import java.io.IOException;

import org.json.simple.JSONObject;

import com.home.model.IpPublic;


public interface ServiceFile {

	
	public void saveFile (IpPublic ipPublic) throws IOException;
	
	public JSONObject getJsonObj (IpPublic ipPublic);
}
