package com.home.service;

import com.home.model.Mail;

public interface ServiceMail {
	
	public void send(Mail mail);

}
