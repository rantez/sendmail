package com.home.model;

import java.net.MalformedURLException;
import java.net.URL;

public class IpPublic {
	
	public URL whatismyip;
	public String ip;
	public String fecha;
	private static IpPublic ipPublic;

	
	 public  static IpPublic getIpPublic() {
		 
		 if (ipPublic==null) {
		 
			 ipPublic=new IpPublic();
		 }
		 return ipPublic;
		 }
	
	
	public IpPublic() {
		try {
			this.whatismyip  = new URL("http://checkip.amazonaws.com");
			this.ip = "";
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public URL getWhatismyip() {
		return whatismyip;
	}
	public void setWhatismyip(URL whatismyip) {
		this.whatismyip = whatismyip;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}


	public String getFecha() {
		return fecha;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	
	
}
