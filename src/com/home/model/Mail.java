package com.home.model;

import com.home.utils.Constantes;

public class Mail {


	public String asunto;// Asunto
	public String cuerpo;// mensaje
	public String mailOrigen;
	public String mailDestino;

	private static Mail mails;

	public static Mail getMail() {
		if (mails == null) {
			mails = new Mail();
		}
		return mails;
	}

	public Mail() {
	
		this.asunto = Constantes.ASUNTO;
		this.cuerpo = Constantes.CUERPO;
		this.mailOrigen = Constantes.MAIL_DE;
		this.mailDestino = Constantes.MAIL_PARA;
	}

	
	

	public String getMailOrigen() {
		return mailOrigen;
	}

	public void setMailOrigen(String mailOrigen) {
		this.mailOrigen = mailOrigen;
	}

	public String getMailDestino() {
		return mailDestino;
	}

	public void setMailDestino(String mailDestino) {
		this.mailDestino = mailDestino;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getCuerpo() {
		return cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}

}
