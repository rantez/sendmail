package com.home.model;

public class ServerFTP {

	public String server;
	public int port;
	public String user;
	public String pass;
	
	
	

	public ServerFTP() {
		this.server = "www.myserver.com";
		this.port = 21;
		this.user = "user";
		this.pass  = "pass";
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

}
