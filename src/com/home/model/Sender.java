package com.home.model;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.home.utils.Constantes;

public class Sender {

	public String servidorSMTP = "smtp.gmail.com";
	public String puertoEnvio = "465";
	public String password = "password";// password dev
	public static String mailAth;
	public static Sender sender;

	public static Sender getSender() {

		if (sender == null) {
			sender = new Sender();
		}
		return sender;
	}

	public Sender() {
		this.password = Constantes.PASSWORD_MAIL;
		this.servidorSMTP = Constantes.SERVIDOR_SMTP;
		this.puertoEnvio = Constantes.PUERTO_ENVIO;
	}



	public String getServidorSMTP() {
		return servidorSMTP;
	}

	public void setServidorSMTP(String servidorSMTP) {
		this.servidorSMTP = servidorSMTP;
	}

	public String getPuertoEnvio() {
		return puertoEnvio;
	}

	public void setPuertoEnvio(String puertoEnvio) {
		this.puertoEnvio = puertoEnvio;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}



}
