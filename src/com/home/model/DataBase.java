package com.home.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.home.utils.Constantes;

public class DataBase {

	private Connection conn = null;
	private Statement stmt = null;
	
	private  String jdbc_driver;  
	private  String db_url;

    //  Database credentials
	private String user;
	private String pass;
	
	private String nameTable;
	
	
	
	private static Connection connection;
	
	
	public static Connection getConnection(){
		
		if(connection==null){
			DataBase dataBase = new DataBase();
			try {
				Class.forName(dataBase.getJdbc_driver());
				System.out.println("Connecting to database...");
				connection = DriverManager.getConnection(dataBase.getDb_url(),dataBase.getUser(),dataBase.getPass());
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return connection;
	}

	
	
	
	
	
	

	public DataBase() {
		this.jdbc_driver = Constantes.DRIVER_MYSQL;
		this.db_url =Constantes.DB_URL;
		this.user =Constantes.USER_MYSQL;
		this.pass =Constantes.PASS_MYSQL;
	}








	public String getJdbc_driver() {
		return jdbc_driver;
	}








	public void setJdbc_driver(String jdbc_driver) {
		this.jdbc_driver = jdbc_driver;
	}








	public String getDb_url() {
		return db_url;
	}








	public void setDb_url(String db_url) {
		this.db_url = db_url;
	}








	public String getUser() {
		return user;
	}








	public void setUser(String user) {
		this.user = user;
	}








	public String getPass() {
		return pass;
	}








	public void setPass(String pass) {
		this.pass = pass;
	}








	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public Statement getStmt() {
		return stmt;
	}

	public void setStmt(Statement stmt) {
		this.stmt = stmt;
	}

}
