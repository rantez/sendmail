package com.home.serviceImpl;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import com.home.model.IpPublic;
import com.home.service.ServiceFile;



@Service("serviceFile")
public class ServiceFileImpl implements ServiceFile {
	

	@Override
	public void saveFile(IpPublic ipPublic) throws IOException {
		JSONObject obj = getJsonObj(ipPublic);
		try (FileWriter file = new FileWriter("jsonState.txt") ){
			file.write(obj.toJSONString());
			System.out.println("Successfully Copied JSON Object to File...");
			System.out.println("\nJSON Object: " + obj);
			System.out.println("\n Jfile.toString() " + file.toString());
		}
	}

	@Override
	public JSONObject getJsonObj(IpPublic ServiceFile) {
		    Date date = new Date();
			JSONObject obj = new JSONObject();
			obj.put("ip", ServiceFile.getIp());
			obj.put("fecha", date.toString());
		return obj;
	}


}
