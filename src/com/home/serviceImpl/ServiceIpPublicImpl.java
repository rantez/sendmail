package com.home.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.stereotype.Service;

import com.home.model.IpPublic;
import com.home.service.ServiceIpPublic;


@Service("serviceIpPublic")
public class ServiceIpPublicImpl implements ServiceIpPublic{

	@Override
	public IpPublic getPublicIp() {
		IpPublic ipPublic = IpPublic.getIpPublic();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(ipPublic.getWhatismyip().openStream()));
			
			ipPublic.setIp(in.readLine()); 
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	System.out.println(ipPublic.getIp());
    	return ipPublic;
	}

}
