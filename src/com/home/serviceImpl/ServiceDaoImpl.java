package com.home.serviceImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;

import org.springframework.stereotype.Service;

import com.home.model.DataBase;
import com.home.model.IpPublic;
import com.home.service.ServiceDao;


@Service("serviceDao")
public class ServiceDaoImpl implements ServiceDao {

	@Override
	public void saveIp(IpPublic ipPublic) throws SQLException {
		Calendar calendar = Calendar.getInstance();
		System.out.println("calendar.getCalendarType()..." + calendar.getCalendarType());
		long startDate = calendar.getTime().getTime();
		Connection conn = DataBase.getConnection();
		Statement stmt = null;
		stmt = conn.createStatement();
		String query = "INSERT INTO securityhome.home ( fecha, ip) VALUES (?,?)";
		PreparedStatement preparedStmt = conn.prepareStatement(query);
		preparedStmt.setTimestamp(1, new Timestamp(startDate));
		System.out.println("Creating startDate..." + startDate);
		preparedStmt.setString(2, ipPublic.getIp());
		preparedStmt.execute();
		stmt.close();
		conn.close();
		System.out.println("Insert ip ----> " + ipPublic.getIp());
	}

}
