package com.home.serviceImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.stereotype.Service;

import com.home.service.ServiceFtp;
import com.home.utils.Constantes;


@Service("serviceFtp")
public class ServiceFtpImple implements ServiceFtp {

	@Override
	public void uploadIp() {
		FTPClient ftpClient = new FTPClient();
		
		
		try {
			ftpClient.connect(Constantes.SERVER_FTP, Constantes.PORT_FTP);
			ftpClient.login(Constantes.USER_FTP, Constantes.PASS_FTP);
			ftpClient.enterLocalPassiveMode();
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			File firstLocalFile = new File("jsonState.txt");
			
			
			String firstRemoteFile = "jsonState.txt";
            InputStream inputStream = new FileInputStream(firstLocalFile);
            
            
            System.out.println("Start uploading first file");
            boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
            inputStream.close();
            if (done) {
                System.out.println("The first file is uploaded successfully.");
            }
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	

}
