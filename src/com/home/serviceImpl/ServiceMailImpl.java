package com.home.serviceImpl;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;

import com.home.model.Mail;
import com.home.model.Sender;
import com.home.service.ServiceMail;
import com.home.utils.Constantes;


@Service("serviceMail")
public class ServiceMailImpl implements ServiceMail {
	
	private static String mailAth;
	private static String pass;

	@Override
	public void send(Mail mail) {
		
		mailAth=mail.getMailOrigen();
		Properties props = Constantes.getProperties(mailAth);
		SecurityManager security = System.getSecurityManager();

		try {
			Authenticator auth = new autentificadorSMTP();
			Session session = Session.getInstance(props, auth);// __________
			session.setDebug(true);

			MimeMessage msg = new MimeMessage(session);
			msg.setText(mail.getCuerpo());
			msg.setSubject(mail.getAsunto());
			msg.setFrom(new InternetAddress(mail.getMailOrigen()));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(mail.getMailDestino()));
			Transport.send(msg);
		} catch (Exception mex) {
			mex.printStackTrace();
		}

	}
	
	
	private class autentificadorSMTP extends javax.mail.Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(mailAth, Sender.getSender().getPassword());
		}
	}

}
